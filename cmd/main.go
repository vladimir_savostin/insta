package main

import (
	gonfig "crypto-darvin-b/pkg/dep/sources/https---github.com-tkanos-gonfig"
	"github.com/astaxie/beego/logs"
	"github.com/go-pg/pg"
	"gopkg.in/ahmdrz/goinsta.v2"
	"insta/bot"
	"insta/conf"
	"insta/db"
	"insta/startegy"
	"time"
)

const settingsPath = "settings.json"

func init() {
	logs.SetLogFuncCall(true)
	logs.SetLogFuncCallDepth(3)
	err := gonfig.GetConf(settingsPath, &conf.Settings)
	if err != nil {
		panic(err)
	}
	err = conf.Settings.Check()
	if err != nil {
		panic(err)
	}
	logs.Debug("%+v", conf.Settings)
	db.DB = pg.Connect(&pg.Options{
		User:     conf.Settings.DB.User,
		Password: conf.Settings.DB.Password,
		Database: conf.Settings.DB.DbName,
	})
	logs.Info("connected to db")
	if db.DB == nil {
		panic("not connected to db")
	}
	_, err = db.DB.Exec("SELECT 1")
	if err != nil {
		panic(err)
	}
}

func main() {
	logs.Debug("start insta bot")
	insta := goinsta.New(
		conf.Settings.Account.Username,
		conf.Settings.Account.Password)
	if err := insta.Login(); err != nil {
		logs.Error(err)
		return
	}
	instaBot := bot.NewInstaBot(insta)
	logs.Debug("%+v", instaBot)
	s := new(startegy.FollowByStarsStrategy)
	s.Setup(conf.Settings.Insta)
	go instaBot.Run(s)
	for {
		time.Sleep(time.Minute)
		logs.Info("**************************************************\n%+v",
			instaBot.Limits)
	}
	//instaBot.FollowByStarsStrategy(insta.Account)
	defer insta.Logout()
}
