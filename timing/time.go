package timing

import (
	"math/rand"
	"time"
)

//RandDuration - возвращает случайный временной промежуток в диапазоне от min до max
func RandDuration(min, max time.Duration) time.Duration {
	rand.Seed(time.Now().UnixNano())
	return time.Duration(rand.Intn(int(max)-int(min)) + int(min))
}
