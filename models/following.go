package models

import (
	"github.com/go-pg/pg"
	"time"
)

//FollowingUsers - пользователи на которых подписан аккаунт
type FollowingUsers struct {
	tableName            struct{} `sql:"following_users"`
	ID                   int64
	Username             string
	IsVerified           bool
	IsBusiness           bool
	IsPrivate            bool
	Gender               *int
	FollowerCount        *int
	FollowingCount       *int
	FollowingTagCount    *int
	MediaCount           *int
	CityName             string
	Category             string
	MutualFollowersCount float64
	FollowingTag         string //тэг, по которому был найден пользователь
	FollowingUser        string //пользователь, блягодаря которому был найден данный пользователь
	EnableUnfollow       bool   //разрешено ли отписываться
	Created              time.Time
	Unfollowed           bool //true если мы отписаны от аккаунта
}

func (u FollowingUsers) Save(db *pg.DB) error {
	u.Created = time.Now()
	_, err := db.Model(&u).Insert()
	return err
}

func (u *FollowingUsers) SelectByID(db *pg.DB, id int64) error {
	return db.Model(u).Where("id=?", id).Select()
}
