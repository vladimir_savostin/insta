package startegy

import (
	"gopkg.in/ahmdrz/goinsta.v2"
	"insta/conf"
)

type Strategy interface {
}

//FollowStrategy - интерфейс стратегии подписок
type FollowStrategy interface {
	Strategy
	Setup(settings conf.Instagram)
	Follow(ctx *Context)
}

type Context struct {
	Insta  *goinsta.Instagram
	Limits *Limits
}
