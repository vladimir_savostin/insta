package startegy

import (
	"fmt"
	"github.com/astaxie/beego/logs"
	"github.com/jinzhu/copier"
	"gopkg.in/ahmdrz/goinsta.v2"
	"insta/conf"
	"insta/db"
	"insta/models"
	"insta/timing"
	"os"
	"time"
)

var errMaxErrCountFollow = fmt.Errorf("max follow error count is reached")

//commonStrategy - стурктура, реализующая общие методы для всех стратегий
type commonStrategy struct {
	ErrorCountFollow int
}

//FollowFirstUsers - подписаться на первые n подписчиков пользователя user
//бот не обязательно подпишется ровно на n аккаунтов, некотрые не подойдут по критериям
func (s *commonStrategy) FollowFirstUsers(ctx *Context, user *goinsta.User, n int) {
	logs.Debug("start follow first %v %s followers", n, user.Username)
	i := 0
	subUsers := user.Followers()
	for subUsers.Next() {
		for _, subUser := range subUsers.Users {
			//logs.Debug(subUser)
			err := s.proceedFollow(ctx, &subUser)
			if err != nil {
				logs.Error(err)
				switch err {
				case errMaxErrCountFollow:
					os.Exit(1)
				}
			}
			logs.Debug("%+v", s)
		}
		i++
		if i >= n {
			logs.Debug("follow to first %v %s followers is complete", n, user.Username)
			return
		}
	}
}

//proceedFollow - проводит все необходимые проверки и обновления
//и при необходимости подписывается на пользователя user
func (s *commonStrategy) proceedFollow(ctx *Context, user *goinsta.User) error {
	//синхронизируем инфо о пользователе
	err := s.sync(user)
	if err != nil {
		return err
	}
	//проверяем, не обрабатывался ли пользователь ранее
	if s.isProceeded(user) {
		logs.Info("%s id=%v is already proceeded", user.Username, user.ID)
		return nil
	}
	//проверяем подходит ли пользователь для подписки
	err = s.needFollow(user)
	if err != nil {
		logs.Debug("%s, id=%v: %v", user.Username, user.ID, err)
		return nil
	}
	err = s.follow(ctx, user)
	if err != nil {
		return err
	}
	result, delay := ctx.Limits.Check()
	switch result {
	case exceededDayLimit:
		logs.Warn("max follow per day is exceeded, sleep for %s", delay)
		time.Sleep(delay)
		ctx.Limits.clearDayFollowCount()
		ctx.Limits.clearHourFollowCount()
		ctx.Limits.resetDay()
		ctx.Limits.resetHour()
	case exceededHourLimit:
		logs.Warn("max follow per hour is exceeded, sleep for %s", delay)
		time.Sleep(delay)
		ctx.Limits.clearHourFollowCount()
		ctx.Limits.resetHour()
	}
	ctx.Limits.CheckTimers()
	return nil
}

//sync - wrapper for user.Sync() с временными задержками
func (s *commonStrategy) sync(user *goinsta.User) error {
	delay := timing.RandDuration(minGetDelay, maxGetDelay)
	logs.Debug("sleep for %s", delay)
	time.Sleep(delay)
	return user.Sync()
}

func (s *commonStrategy) follow(ctx *Context, user *goinsta.User) error {
	delay := timing.RandDuration(minPostDelay, maxPostDelay)
	logs.Debug("sleep for %s", delay)
	time.Sleep(delay)
	err := user.Follow()
	if err != nil { //если ошибка, проверим счетчик ошибок по подпискам
		s.incErrorCountFollow()
		if s.ErrorCountFollow >= maxErrorCountFollow {
			return errMaxErrCountFollow
		}
		return err
	} else { //если все норм
		//подбивем все счетчики
		s.clearErrorCountFollow()
		ctx.Limits.incFollowerCount()
		logs.Info("follow %s", user.Username)
		//сохраняем пользователя в базу
		userModel := models.FollowingUsers{}
		err = copier.Copy(&userModel, &user)
		if err != nil {
			return err
		}
		userModel.EnableUnfollow = true
		userModel.FollowingUser = user.Username
		logs.Debug("follow %s id=%v", user.Username, user.ID)
		return userModel.Save(db.DB)
	}
}

//isProceeded - проверяет обрабатывался ли данный пользователь ранее
func (s *commonStrategy) isProceeded(user *goinsta.User) bool {
	userModel := models.FollowingUsers{}
	err := userModel.SelectByID(db.DB, user.ID)
	if err != nil {
		logs.Error(err)
		return false
	}
	if userModel.Username != "" {
		return true
	}
	return true
}

//checkUser - проверяет подходит ли пользователь для того, чтобы подписаться на него
func (s *commonStrategy) needFollow(user *goinsta.User) error {
	if user.FollowerCount < conf.Settings.Bot.MinFollowersToFollow {
		return fmt.Errorf("not follow, have %v followers, min:%v followers",
			user.FollowerCount, conf.Settings.Bot.MinFollowersToFollow)
	}
	if user.FollowerCount > conf.Settings.Bot.MaxFollowersToFollow {
		return fmt.Errorf("not follow, have %v followers, max:%v followers",
			user.FollowerCount, conf.Settings.Bot.MaxFollowersToFollow)
	}
	if user.IsBusiness && !conf.Settings.Bot.FollowBusiness {
		return fmt.Errorf("not follow, this is business account")
	}
	if user.IsPrivate && !conf.Settings.Bot.FollowPrivate {
		return fmt.Errorf("not follow, this is private account")
	}

	return nil
}

//incErrorCountFollow - увеличивает счетчик ошибок неудачной подписки
func (s *commonStrategy) incErrorCountFollow() {
	s.ErrorCountFollow++
}

//clearErrorCountFollow - сбрасывает счетчик ошибок неудачной подписки
func (s *commonStrategy) clearErrorCountFollow() {
	s.ErrorCountFollow = 0
}
