package startegy

import (
	"github.com/astaxie/beego/logs"
	"insta/conf"
	"math/rand"
	"os"
)

type FollowByStarsStrategy struct {
	StarAccounts []string //список аккаунтов звезд, котрые предстоит обрабатывать
	CurrentStar  string   //аккаунт звезды, котрая обрабатывается в текущий момент
	commonStrategy
}

func (s *FollowByStarsStrategy) Setup(settings conf.Instagram) {
	s.StarAccounts = settings.Stars
}

func (s *FollowByStarsStrategy) Follow(ctx *Context) {
	logs.Warn("start follow by stars:")
	for _, starAccount := range s.StarAccounts {
		s.CurrentStar = starAccount
		logs.Info("start proceed star account: %s", starAccount)
		starUser, err := ctx.Insta.Profiles.ByName(starAccount)
		if err != nil {
			logs.Error(err)
			os.Exit(1)
		}
		err = starUser.Sync()
		if err != nil {
			logs.Error(err)
			os.Exit(1)
		}
		//рандомно генерируем число первых n подписчиков звезды, котрых будем обрабатывать
		firstFollowersCount := rand.
			Intn(conf.Settings.Insta.MaxStarFollowerForFollow-conf.Settings.Insta.MinStarFollowerForFollow) +
			conf.Settings.Insta.MinStarFollowerForFollow
		s.FollowFirstUsers(ctx, starUser, firstFollowersCount)
	}
}
