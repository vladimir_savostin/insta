package startegy

import (
	"github.com/astaxie/beego/logs"
	"insta/conf"
	"time"
)

const exceededHourLimit = "hour_limit" //уведомление о превышении часового лимита
const exceededDayLimit = "day_limit"   //уведомление о превышении дневного лимита

const (
	maxErrorCountFollow = 2
	minPostDelay        = time.Second * 45
	maxPostDelay        = time.Second * 160
	minGetDelay         = time.Millisecond * 800
	maxGetDelay         = time.Millisecond * 3000
)

//Limits - данные хранящие количество действий за период и начало преиода
type Limits struct {
	HourStart time.Time //время начала текущего часа
	DayStart  time.Time //время начала текущего дня

	HourFollowerCount int //количество подписок/отписок за последний час
	DayFollowerCount  int //количество подписок/отписок за последний день
}

//Check - проверяет, не превышены ли лимиты api
//если да, возвращает задержку, на которую надо заснуть
func (l *Limits) Check() (result string, delay time.Duration) {
	if l.DayFollowerCount >= conf.Settings.Insta.MaxFollowPerDay {
		//отнимаем из суток время прошедшее со старта текущего дня
		//такая должна быть задержка чтобы заснуть до начала следующего дня
		delay = 24*time.Hour - time.Since(l.DayStart)
		return exceededDayLimit, delay
	}
	if l.HourFollowerCount >= conf.Settings.Insta.MaxFollowPerHour {
		logs.Warn("max follow per hour is exceeded")
		//отнимаем из часа время прошедшее со старта текущего часа
		//такая должна быть задержка чтобы заснуть до начала следующего часа
		delay = time.Hour - time.Since(l.HourStart)
		return exceededHourLimit, delay
	}
	return "", 0
}

//CheckTimers - проверяет начало часа и дня
//если час/день истек, начинает следующий
func (l *Limits) CheckTimers() {
	if time.Since(l.DayStart) > 24*time.Hour {
		l.resetDay()
	}
	if time.Since(l.HourStart) > time.Hour {
		l.resetHour()
	}
}

//incFollowerCount - увеличивает счетчик пользователей, на которых мы подписались/отписались
func (l *Limits) incFollowerCount() {
	l.HourFollowerCount++
	l.DayFollowerCount++
}

//clearHourFollowCount - сбрасывает счетчик пользователей, на которых мы подписались за последний час
func (l *Limits) clearHourFollowCount() {
	l.HourFollowerCount = 0
}

//clearDayFollowCount - сбрасывает счетчик пользователей, на которых мы подписались за последний день
func (l *Limits) clearDayFollowCount() {
	l.DayFollowerCount = 0
}

//resetDay - перезапускает начало дня
func (l *Limits) resetDay() {
	logs.Debug("reset day")
	l.DayStart = time.Now()
}

//resetHour - перезапускает начало часа
func (l *Limits) resetHour() {
	logs.Debug("reset hour")
	l.HourStart = time.Now()
}
