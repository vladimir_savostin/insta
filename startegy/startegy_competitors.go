package startegy

import (
	"github.com/astaxie/beego/logs"
	"insta/conf"
	"math/rand"
	"os"
)

//FollowByCompetitorsStrategy - подписывается на несколько первых подписчиков конкурентов
type FollowByCompetitorsStrategy struct {
	CompetitorAccounts []string //список аккаунтов звезд, котрые предстоит обрабатывать
	CurrentCompetitor  string   //аккаунт звезды, котрая обрабатывается в текущий момент
	commonStrategy
}

func (s *FollowByCompetitorsStrategy) Setup(settings conf.Instagram) {
	s.CompetitorAccounts = settings.Competitors
}

func (s *FollowByCompetitorsStrategy) Follow(ctx *Context) {
	logs.Warn("start follow by competitors:")
	for _, competitorAccount := range s.CompetitorAccounts {
		s.CurrentCompetitor = competitorAccount
		logs.Info("start proceed competitor account: %s", competitorAccount)
		competitorUser, err := ctx.Insta.Profiles.ByName(competitorAccount)
		if err != nil {
			logs.Error(err)
			os.Exit(1)
		}
		err = competitorUser.Sync()
		if err != nil {
			logs.Error(err)
			os.Exit(1)
		}
		//рандомно генерируем число первых n подписчиков звезды, котрых будем обрабатывать
		firstFollowersCount := rand.
			Intn(conf.Settings.Insta.MaxCompetitorFollowerForFollow-conf.Settings.Insta.MinCompetitorFollowerForFollow) +
			conf.Settings.Insta.MinCompetitorFollowerForFollow
		s.FollowFirstUsers(ctx, competitorUser, firstFollowersCount)
	}
}
