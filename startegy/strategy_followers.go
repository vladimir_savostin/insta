package startegy

import (
	"github.com/astaxie/beego/logs"
	"insta/conf"
	"os"
	"time"
)

//FollowByFollowersStrategy - в качесвте базы новых пользователей,
// на которых будем подписываться
//беруться пользователи которые подписаны на наших подписчиков
type FollowByFollowersStrategy struct {
	commonStrategy
}

func (s *FollowByFollowersStrategy) Setup(settings conf.Instagram) {
}

func (s *FollowByFollowersStrategy) FollowByFollowers(ctx *Context) {
	users := ctx.Insta.Account.Followers()
	for users.Next() {
		for _, user := range users.Users {
			logs.Debug("proceed my follower:%s", user.Username)
			subUsers := user.Followers()
			logs.Debug(subUsers.Users)
			for subUsers.Next() {
				for _, subUser := range subUsers.Users {
					//синхронизируем инфо о пользователе
					err := s.sync(&subUser)
					if err != nil {
						logs.Error(err)
						continue
					}
					//проверяем, не обрабатывался ли пользователь ранее
					if s.isProceeded(&subUser) {
						logs.Info("%s id=%v is already proceeded", subUser.Username, subUser.ID)
						continue
					}
					//проверяем подходит ли пользователь для подписки
					err = s.needFollow(&subUser)
					if err != nil {
						logs.Debug("%s, id=%v: %v", subUser.Username, subUser.ID, err)
						continue
					}
					err = s.follow(ctx, &subUser)
					if err != nil {
						logs.Error(err)
						switch err {
						case errMaxErrCountFollow:
							os.Exit(1)
						}
					}
					result, delay := ctx.Limits.Check()
					switch result {
					case exceededDayLimit:
						logs.Warn("max follow per day is exceeded, sleep for %s", delay)
						time.Sleep(delay)
						ctx.Limits.clearDayFollowCount()
						ctx.Limits.clearHourFollowCount()
						ctx.Limits.resetDay()
						ctx.Limits.resetHour()
					case exceededHourLimit:
						logs.Warn("max follow per hour is exceeded, sleep for %s", delay)
						time.Sleep(delay)
						ctx.Limits.clearHourFollowCount()
						ctx.Limits.resetHour()
					}
					ctx.Limits.CheckTimers()
				}
			}
		}
	}
}
