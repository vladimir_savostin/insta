package conf

import "fmt"

var Settings Conf

type Conf struct {
	Account Account   `json:"account"`
	DB      DB        `json:"database"`
	Bot     InstaBot  `json:"bot"`
	Insta   Instagram `json:"instagram"`
}

//Check - проверяет все ли настройки указаны
func (c Conf) Check() error {
	if c.Account.Username == "" {
		return fmt.Errorf("not provided username")
	}
	if c.Account.Password == "" {
		return fmt.Errorf("not provided password")
	}
	if c.Insta.MaxFollowersToFollow == 0 {
		return fmt.Errorf("not provided MaxFollowersToFollow")
	}
	if c.Insta.MinFollowersToFollow == 0 {
		return fmt.Errorf("not provided MinFollowersToFollow")
	}
	if c.Insta.MaxFollowingToFollow == 0 {
		return fmt.Errorf("not provided MaxFollowingToFollow")
	}
	if c.Insta.MinFollowingToFollow == 0 {
		return fmt.Errorf("not provided MinFollowingToFollow")
	}
	if c.Insta.MaxFollowingCount == 0 {
		return fmt.Errorf("not provided MaxFollowingCount")
	}
	if c.Insta.MaxFollowPerHour == 0 {
		return fmt.Errorf("not provided MaxFollowPerHour")
	}
	if c.Insta.MaxFollowPerDay == 0 {
		return fmt.Errorf("not provided MaxFollowPerDay")
	}
	if c.Insta.MaxLikePerHour == 0 {
		return fmt.Errorf("not provided MaxLikePerHour")
	}
	if c.Insta.MaxLikePerDay == 0 {
		return fmt.Errorf("not provided MaxLikePerDay")
	}
	if c.Insta.MaxCommentPerHour == 0 {
		return fmt.Errorf("not provided MaxCommentPerHour")
	}
	if c.Insta.MaxCommentPerDay == 0 {
		return fmt.Errorf("not provided MaxCommentPerDay")
	}
	return nil
}

type Account struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type DB struct {
	User     string `json:"user"`
	Password string `json:"password"`
	DbName   string `json:"dbname"`
}

type InstaBot struct {
}

//Instagram - настройки самого инстаграма (аккаунты, теги и т.д.)
type Instagram struct {
	Stars       []string `json:"stars"`       //список аккаунтов звезд
	Competitors []string `json:"competitors"` //список аккаунтов конкурентов

	//настройки выбора пользователей, на кого будем подписываться
	//следующие две настройки определяют диапазон количества подписчиков у случайного пользователя
	//если количество подписчиков у случайного пользователя не входит в диапазон, то не подписываемся на него
	MaxFollowersToFollow int `json:"max_followers_to_follow"` //макс число подписчиков пользователя, если больше - не подписываемся на него
	MinFollowersToFollow int `json:"min_followers_to_follow"` //мин число подписчиков пользователя, если меньше - не подписываемся на него

	//следующие две настройки определяют диапазон количества аккаунтов, на которые подписан случайный пользователь
	//если количество аккаунтов не входит в диапазон, то не подписываемся на него
	MaxFollowingToFollow int `json:"max_following_to_follow"` //макс число на кого подписан пользователь, больше - не подписываемся
	MinFollowingToFollow int `json:"min_following_to_follow"` //мин число на кого подписан пользователь, больше - не подписываемся

	//следующие настройки определяют диапазон количества первых n подписчиков звезд, на котрых будем подписываться
	//верхняя и нижняя границы нужны лишь для имитации поведения реального пользователя
	//(у каждой звезды будет отобрано разное количество подписичков)
	MaxStarFollowerForFollow int `json:"max_star_follower_for_follow"` //максимальное число первых n подписчиков звезд, на которых подписываемся
	MinStarFollowerForFollow int `json:"min_star_follower_for_follow"` //минимальное число первых n подписчиков звезд, на которых подписываемся

	//следующие настройки определяют на какие типы аккаунтов разрешено подписываться
	FollowBusiness bool `json:"follow_business"` //если true, то подписываемся даже на бизнес аккаунты
	FollowPrivate  bool `json:"follow_private"`  //если true, то подписываемся даже на приватные аккаунты

	//настройки лимитов api инстаграм
	MaxFollowingCount int `json:"max_following_count"`  //макс число на сколько аккаунтов можно подписаться
	MaxFollowPerHour  int `json:"max_follow_per_hour"`  //макс число подписок+отписок в час
	MaxFollowPerDay   int `json:"max_follow_per_day"`   //макс число подписок+отписок в день
	MaxLikePerHour    int `json:"max_like_per_hour"`    //макс число лайков в час
	MaxLikePerDay     int `json:"max_like_per_day"`     //макс число лайков в день
	MaxCommentPerHour int `json:"max_comment_per_hour"` //макс число комментариев в час
	MaxCommentPerDay  int `json:"max_comment_per_day"`  //макс число комментариев в день
}
