package bot

import (
	"gopkg.in/ahmdrz/goinsta.v2"
	"insta/startegy"
	"time"
)

type InstaBot struct {
	insta              *goinsta.Instagram
	strategies         []startegy.FollowStrategy //список стратегий
	ErrorCountFollow   int                       //счетчик ошибок на подписку
	ErrorCountUnfollow int                       //счетчик ошибок на отписку

	Limits *startegy.Limits
}

func NewInstaBot(insta *goinsta.Instagram) *InstaBot {
	b := new(InstaBot)
	b.insta = insta
	b.ErrorCountFollow = 0
	b.ErrorCountUnfollow = 0
	b.Limits = new(startegy.Limits)
	b.Limits.HourStart = time.Now()
	b.Limits.DayStart = time.Now()
	b.Limits.HourFollowerCount = 0
	b.Limits.DayFollowerCount = 0
	return b
}

func (b *InstaBot) Run(strategy startegy.FollowStrategy) {
	strategy.Follow(&startegy.Context{
		Insta:  b.insta,
		Limits: b.Limits,
	})
}
